var express = require('express');
var path = require('path');
var app = express();

var router = require('./routes/index');

app.set('port', (process.env.PORT || 5000));

// Set static directory as public
app.use(express.static(__dirname + '/public'));

// Set views folder as folder for templates
app.set('views', './views');

// Set html engine using ejs
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// Use router for request rewrite
app.use('/', router);

app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});


