var express = require('express');
var router = express.Router();

/* GET index page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Visual Story Telling' });
});

/* GET home page. */
router.get('/home', function(req, res, next) {
    res.render('home', { title: 'Visual Story Telling' });
});

module.exports = router;
